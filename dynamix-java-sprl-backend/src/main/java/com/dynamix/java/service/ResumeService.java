package com.dynamix.java.service;

import com.dynamix.java.exceptions.ResourceNotFoundException;
import com.dynamix.java.model.Resume;
import com.dynamix.java.model.Technology;

import java.time.LocalDate;
import java.util.List;

public interface ResumeService {

    Resume getResumeByConsultantId(Long id) throws ResourceNotFoundException;

    List<Resume> getAllResumes();

    List<Resume> getResumeByReceptionDate(LocalDate date) throws ResourceNotFoundException;

    Resume saveResume(Resume resume);

    void deleteResume(Long id) throws ResourceNotFoundException;

    Technology getTechnologyById(Long id) throws ResourceNotFoundException;

    Technology saveTechnology(Technology technology);

    void deleteTechnology(Long id) throws ResourceNotFoundException;
}
