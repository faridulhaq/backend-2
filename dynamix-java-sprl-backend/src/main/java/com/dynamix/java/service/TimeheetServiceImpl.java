package com.dynamix.java.service;

import java.time.LocalDate;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.dynamix.java.exceptions.ResourceNotFoundException;
import com.dynamix.java.model.Timesheet;
import com.dynamix.java.repository.TimesheetRepository;


@Service
public class TimeheetServiceImpl implements TimesheetService {
	
	@Autowired
	private TimesheetRepository timesheetRepository;

	@Override
	public List<Timesheet> getAllTimesheets() {
		return timesheetRepository.findAll();
	}

	@Override
	public Timesheet getTimesheetByMissionIdAndMonth(Long missionId, LocalDate month) {
		return timesheetRepository.findByMonthAndMission_id(month, missionId);
	}
	
	@Override
	public List<Timesheet> getTimesheetByMissionId(Long missionId) {
		return timesheetRepository.findByMission_id(missionId);
	}

	@Override
	public Timesheet createTimesheet(Timesheet timesheet) {
		return timesheetRepository.save(timesheet);
	}

	@Override
	public Timesheet updateTimesheet(Long timesheetId, Timesheet timesheetDetails) throws ResourceNotFoundException {
		Timesheet timesheet = timesheetRepository.findById(timesheetId)
				.orElseThrow(() -> new ResourceNotFoundException("Timesheet not found for this id :: " + timesheetId));
		
		timesheet.setMonth(timesheetDetails.getMonth());
		timesheet.setUnit(timesheetDetails.getUnit());
		timesheet.setUnitNbr(timesheetDetails.getUnitNbr());
		
		final Timesheet updatedTimesheet = timesheetRepository.save(timesheet);
		return updatedTimesheet;
	}

	@Override
	public void deleteTimesheet(Long timesheetId) throws ResourceNotFoundException {
		Timesheet timesheet = timesheetRepository.findById(timesheetId)
			    .orElseThrow(() -> new ResourceNotFoundException("Timesheet not found for this id :: " + timesheetId));
		timesheetRepository.delete(timesheet);
	}

	
}
