package com.dynamix.java.service;

import com.dynamix.java.exceptions.ResourceNotFoundException;
import com.dynamix.java.model.Consultant;

import java.util.List;

public interface ConsultantService {

    List<Consultant> getAllConsultants();

    Consultant getConsultantById(Long id) throws ResourceNotFoundException;

    Consultant saveConsultant(Long employeeId, Consultant consultant) throws ResourceNotFoundException;
    
    Consultant addConsultant(Consultant consultant) throws Exception;
    
    Consultant updateConsultant(Long consultantId, Consultant consultantDetails) throws ResourceNotFoundException;

    void deleteConsultant(Long id) throws ResourceNotFoundException;
}
