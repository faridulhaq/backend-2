package com.dynamix.java.service;

import com.dynamix.java.exceptions.ResourceNotFoundException;
import com.dynamix.java.model.Contact;
import com.dynamix.java.repository.ContactRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class ContactServiceImpl implements ContactService {

    @Autowired
    private ContactRepository contactRepository;

    @Override
    public List<Contact> getAllContacts() {
        return contactRepository.findAll();
    }

    @Override
    public Contact getContactById(Long id) throws ResourceNotFoundException {
        return contactRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("Contact not found for this id :: " + id));
    }

    @Override
    public Contact saveContact(Contact contact) {
        return contactRepository.save(contact);
    }

    @Override
    public void deleteContact(Long id) throws ResourceNotFoundException {
        Contact contact = getContactById(id);
        contactRepository.delete(contact);
    }


}
