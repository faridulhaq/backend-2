package com.dynamix.java.repository;

import java.time.LocalDate;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.dynamix.java.model.Timesheet;

@Repository
public interface TimesheetRepository extends JpaRepository<Timesheet, Long>{

	Timesheet findByMonthAndMission_id(LocalDate month,long missionId);
	
	List<Timesheet> findByMission_id(long missionId);
}
