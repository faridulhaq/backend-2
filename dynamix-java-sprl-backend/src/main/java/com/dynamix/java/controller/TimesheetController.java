package com.dynamix.java.controller;

import java.time.LocalDate;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.dynamix.java.exceptions.ResourceNotFoundException;
import com.dynamix.java.model.Timesheet;
import com.dynamix.java.service.TimesheetService;

@CrossOrigin(origins = "http://localhost:4200")
@RestController
@RequestMapping("/api/v1/timesheet")
public class TimesheetController {
	@Autowired
	private TimesheetService timesheetService;
	
	@GetMapping
	public List<Timesheet> getAllTimesheets() {
		return timesheetService.getAllTimesheets();
	}

	@GetMapping("/month/{missionId}")
	public ResponseEntity<Timesheet> getTimesheetByMissionIdAndMonth(@PathVariable(value = "missionId") Long missionId,
			                         @RequestParam("month")   @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate month) {
		
		Timesheet timesheet = timesheetService.getTimesheetByMissionIdAndMonth(missionId,month);
		return ResponseEntity.ok(timesheet);
	}
	
	@GetMapping("/{missionId}")
	public ResponseEntity<List<Timesheet>> getTimesheet(@PathVariable(value = "missionId") Long missionId) {
		
		List<Timesheet> timesheet = timesheetService.getTimesheetByMissionId(missionId);
		return ResponseEntity.ok(timesheet);
	}

	@PostMapping
	public Timesheet createTimesheet(@Valid @RequestBody Timesheet timesheet) {
		return timesheetService.createTimesheet(timesheet);
	}

	@PutMapping("/{id}")
	public ResponseEntity<Timesheet> updateTimesheet(@PathVariable(value = "id") Long timesheetId,
			@Valid @RequestBody Timesheet timesheetDetails) throws ResourceNotFoundException {
		Timesheet updatedTimesheet = timesheetService.updateTimesheet(timesheetId, timesheetDetails);
		return ResponseEntity.ok(updatedTimesheet);
	}

	@DeleteMapping("/{id}")
	public Map<String, Boolean> deleteTimesheet(@PathVariable(value = "id") Long timesheetId)
			throws ResourceNotFoundException {
		timesheetService.deleteTimesheet(timesheetId);;
		Map<String, Boolean> response = new HashMap<>();
		response.put("deleted", Boolean.TRUE);
		return response;
	}
}
