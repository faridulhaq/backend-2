package com.dynamix.java.controller;

import com.dynamix.java.exceptions.ResourceNotFoundException;
import com.dynamix.java.model.Resume;
import com.dynamix.java.model.Technology;
import com.dynamix.java.service.ResumeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.time.LocalDate;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/api/v1")
public class ResumeController {

    @Autowired
    private ResumeService resumeService;

    @GetMapping("/resume/getByConsultantId/{consultantId}")
    public Resume getResumeByConsultantId(@PathVariable(value = "consultantId") Long consultantId) throws ResourceNotFoundException {
        return resumeService.getResumeByConsultantId(consultantId);
    }

    @GetMapping("/resume/all")
    public List<Resume> getAllResumes() {
        return resumeService.getAllResumes();
    }

    @GetMapping("/resume/{date}")
    public List<Resume> getResumeByReceptionDate(@DateTimeFormat(pattern = "yyyy-MM-dd") @PathVariable("date") LocalDate date) throws ResourceNotFoundException {
        return resumeService.getResumeByReceptionDate(date);
    }

    @PostMapping("/resume/save")
    public Resume saveResume(@Valid @RequestBody Resume resume) {
        return resumeService.saveResume(resume);
    }

    @GetMapping("/technology/{id}")
    public Technology getTechnologyById(@PathVariable("id") Long id) throws ResourceNotFoundException {
        return resumeService.getTechnologyById(id);
    }

    @PostMapping("/technology/save")
    public Technology saveTechnology(@Valid @RequestBody Technology technology) {
        return resumeService.saveTechnology(technology);
    }

    @DeleteMapping("/technology/{id}")
    public Map<String, Boolean> deleteTechnology(@PathVariable("id") Long id) throws ResourceNotFoundException {
        resumeService.deleteTechnology(id);
        Map<String, Boolean> response = new HashMap<>();
        response.put("deleted", Boolean.TRUE);
        return response;
    }
}
