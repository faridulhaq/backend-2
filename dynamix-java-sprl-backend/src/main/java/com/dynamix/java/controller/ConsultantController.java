package com.dynamix.java.controller;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.dynamix.java.exceptions.ResourceNotFoundException;
import com.dynamix.java.model.Consultant;
import com.dynamix.java.service.ConsultantService;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@RestController
@RequestMapping("/api/v1/consultant")
public class ConsultantController {

    @Autowired
    private ConsultantService consultantService;

    @GetMapping("/all")
    public List<Consultant> getAllConsultants() {
        return consultantService.getAllConsultants();
    }

    @GetMapping("/{id}")
    public ResponseEntity<Consultant> getConsultantById(@PathVariable(value = "id") Long id) throws ResourceNotFoundException {
        return ResponseEntity.ok().body(consultantService.getConsultantById(id));
    }

    @PostMapping("/save/{employeeId}")
    public Consultant saveConsultant(@PathVariable (value = "employeeId") Long employeeId, @Valid @RequestBody Consultant consultant) throws ResourceNotFoundException {
        return consultantService.saveConsultant(employeeId, consultant);
    }
    
    @PostMapping(value = "/add", consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    public ResponseEntity<Consultant>  addConsultant( @RequestParam("consultant") String consultant, 
    		                         @RequestParam(value = "photo", required=false) final MultipartFile photo) throws Exception {
        
    	Consultant consultantObj = new ObjectMapper().readValue(consultant, Consultant.class); 
    	consultantObj.setPhoto(photo.getBytes());
    	return ResponseEntity.ok().body(consultantService.addConsultant(consultantObj));
    }
    
    @PutMapping(value = "/{consultantId}",  consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    public ResponseEntity<Consultant> updateConsultant(@PathVariable(value = "consultantId") Long consultantId,
    		@RequestParam("consultant") String consultantDetails, @RequestParam(value = "photo", required=false) final MultipartFile photo) throws ResourceNotFoundException, JsonParseException, JsonMappingException, IOException {
        
    	Consultant consultantObj = new ObjectMapper().readValue(consultantDetails, Consultant.class); 
    	consultantObj.setPhoto(photo.getBytes());
    	return ResponseEntity.ok().body(consultantService.updateConsultant(consultantId,consultantObj));
    }

    @DeleteMapping("/{id}")
    public Map<String, Boolean> deleteConsultant(@PathVariable(value = "id") Long id)
            throws ResourceNotFoundException {
        consultantService.deleteConsultant(id);
        Map<String, Boolean> response = new HashMap<>();
        response.put("deleted", Boolean.TRUE);
        return response;
    }
}
