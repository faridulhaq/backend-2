package com.dynamix.java.util;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;
import java.util.stream.Stream;

public enum Gender {
    MALE(Values.MALE), FEMALE(Values.FEMALE);

    private String value;

    Gender(final String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    public static class Values {
        public static final String MALE = "Male";
        public static final String FEMALE = "Female";
    }

    @Converter(autoApply = true)
    public static class AttibuteValueConverter implements AttributeConverter<Gender, String> {

        @Override
        public String convertToDatabaseColumn(Gender gender) {
            if (gender == null) {
                return null;
            }
            return gender.getValue();
        }

        @Override
        public Gender convertToEntityAttribute(String value) {
            if (value == null) {
                return null;
            }
            return Stream.of(Gender.values())
                    .filter(c -> c.getValue().equals(value))
                    .findFirst()
                    .orElseThrow(IllegalArgumentException::new);
        }
    }
}