package com.dynamix.java.model;

import javax.persistence.*;

@Entity
@Table(name = "technology")
public class Technology {

    @Id
    @Column(name = "technology_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long technologyId;

    @Column(name = "name", nullable = false)
    private String name;

    public long getTechnologyId() {
        return technologyId;
    }

    public void setTechnologyId(long technologyId) {
        this.technologyId = technologyId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
