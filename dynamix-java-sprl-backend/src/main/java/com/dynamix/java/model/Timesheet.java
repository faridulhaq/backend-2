package com.dynamix.java.model;

import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import com.fasterxml.jackson.annotation.JsonFormat;

@Entity
@Table(name = "timesheet")
public class Timesheet {

	private long timesheetId;
	private LocalDate month;
	private String unit;
	private long unitNbr;
	private Mission mission;

	public Timesheet() {

	}

	public Timesheet(long unitNbr, Mission mission) {
		this.unitNbr = unitNbr;
		this.mission = mission;
	}

	public Timesheet(long unitNbr, Mission mission, LocalDate month, String unit) {
		this(unitNbr, mission);
		this.month = month;
		this.unit = unit;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "timesheetId")
	public long getTimesheetId() {
		return timesheetId;
	}

	public void setTimesheetId(long timesheetId) {
		this.timesheetId = timesheetId;
	}

	@Column(name = "month")
	@JsonFormat(pattern = "dd/MM/yyyy")
	public LocalDate getMonth() {
		return month;
	}

	public void setMonth(LocalDate month) {
		this.month = month;
	}

	@Column(name = "unit")
	public String getUnit() {
		return unit;
	}

	public void setUnit(String unit) {
		this.unit = unit;
	}

	@Column(name = "unitNbr", nullable = false)
	public long getUnitNbr() {
		return unitNbr;
	}

	public void setUnitNbr(long unitNbr) {
		this.unitNbr = unitNbr;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "missionId", referencedColumnName = "id", nullable = false)
	@OnDelete(action = OnDeleteAction.CASCADE)
	public Mission getMission() {
		return mission;
	}

	public void setMission(Mission mission) {
		this.mission = mission;
	}

	@Override
	public String toString() {
		return "Timesheet [timesheetId=" + timesheetId + ", month=" + month + ", unit=" + unit + ", unitNbr="
				+ unitNbr + ", mission=" + mission + "]";
	}

}
