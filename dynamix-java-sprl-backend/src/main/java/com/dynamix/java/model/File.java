package com.dynamix.java.model;

import com.dynamix.java.util.FileType;
import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.*;

@Entity
@Table(name = "file")
public class File {

	private long fileId;
	private String location;
	private float size;
	private FileType fileType;
	@JsonIgnore
	private Resume resume;

	public File() {

	}

	public File(String location, float size, FileType fileType, Resume resume) {
		this.location = location;
		this.size = size;
		this.fileType = fileType;
		this.resume=resume;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "fileId")
	public long getFileId() {
		return fileId;
	}

	public void setFileId(long fileId) {
		this.fileId = fileId;
	}

	@Column(name = "location", nullable = false)
	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	@Column(name = "size", nullable = false)
	public float getSize() {
		return size;
	}

	public void setSize(float size) {
		this.size = size;
	}

	@Column(columnDefinition ="Enum('"+ FileType.Values.PDF+"', '"+FileType.Values.MSWORD+"')", nullable = false)
	@Convert(converter = FileType.FileTypeConverter.class)
	public FileType getFileType() {
		return fileType;
	}

	public void setFileType(FileType fileType) {
		this.fileType = fileType;
	}

	@ManyToOne(fetch = FetchType.LAZY, optional = false,cascade = CascadeType.PERSIST)
	@JoinColumn(name="resumeId",referencedColumnName = "resumeId",nullable=false)
	@OnDelete(action = OnDeleteAction.CASCADE)
	public Resume getResume() {
		return resume;
	}

	public void setResume(Resume resume) {
		this.resume = resume;
	}

	@Override
	public String toString() {
		return "File [fileId=" + fileId + ", location=" + location + ", size=" + size + ", fileType=" + fileType
				+ ", resume=" + resume + "]";
	}
}
