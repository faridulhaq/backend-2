package com.dynamix.java.model;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

import com.dynamix.java.util.CnyType;

@Entity
@Table(name = "company")
public class Company {

	private long companyId;
	private String name;
	private long vatNumber;
	private String comments;
	private CnyType cnyType;
	private Address address;

	public Company() {

	}

	public Company(long vatNumber, Address address) {
		this.vatNumber = vatNumber;
		this.address = address;
	}

	public Company(String name, long vatNumber, String comments, CnyType cnyType, Address address) {
		this(vatNumber,address);
		this.name = name;
		this.comments = comments;
		this.cnyType = cnyType;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO, generator = "native")
	@GenericGenerator(name = "native", strategy = "native")
	@Column(name = "companyId")
	public long getCompanyId() {
		return companyId;
	}

	public void setCompanyId(long companyId) {
		this.companyId = companyId;
	}

	@Column(name = "name")
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Column(name = "vatNumber", nullable = false)
	public long getVatNumber() {
		return vatNumber;
	}

	public void setVatNumber(long vatNumber) {
		this.vatNumber = vatNumber;
	}

	@Column(name = "comments")
	public String getComments() {
		return comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}

	@Column(columnDefinition ="Enum('"+ CnyType.Values.CLIENT+"', '"+CnyType.Values.PROSPECT+"', '"+CnyType.Values.END_CLIENT+"')")
	@Convert(converter = CnyType.CnyTypeConverter.class)
	public CnyType getCnyType() {
		return cnyType;
	}

	public void setCnyType(CnyType cnyType) {
		this.cnyType = cnyType;
	}

	@OneToOne(fetch = FetchType.LAZY, optional = false, cascade = CascadeType.ALL)
	@JoinColumn(name = "addressId" , nullable = false)
	public Address getAddress() {
		return address;
	}

	public void setAddress(Address address) {
		this.address = address;
	}

	@Override
	public String toString() {
		return "Company [companyId=" + companyId + ", name=" + name + ", vatNumber=" + vatNumber + ", comments="
				+ comments + ", cnyType=" + cnyType + ", address=" + address + "]";
	}
}
