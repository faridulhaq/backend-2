package com.dynamix.java.model;

import java.time.LocalDate;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Transient;

import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity(name = "contract")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler", "missions"})
public class Contract {

    private Long id;
    private LocalDate startDate;
    private LocalDate endDate;

    private Company company;
    
    private Company endClient;

    private Consultant consultant;
    
	private Set<Mission> missions;
    
    public Contract() {
    }

    public Contract(LocalDate startDate, LocalDate endDate, Company company) {
        this.startDate = startDate;
        this.endDate = endDate;
        this.company = company;
    }

    @Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @JsonFormat(pattern = "dd/MM/yyyy")
    @Column(nullable = false)
    public LocalDate getStartDate() {
        return startDate;
    }

    public void setStartDate(LocalDate startDate) {
        this.startDate = startDate;
    }

    @JsonFormat(pattern = "dd/MM/yyyy")
    @Column(nullable = false)
    public LocalDate getEndDate() {
        return endDate;
    }

    public void setEndDate(LocalDate endDate) {
        this.endDate = endDate;
    }

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
	@JoinColumn(name = "company_id",referencedColumnName="companyId", nullable = false)
	@OnDelete(action = OnDeleteAction.CASCADE)
    public Company getCompany() {
        return company;
    }

    public void setCompany(Company company) {
        this.company = company;
    }

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
	@JoinColumn(name = "endclient_id",referencedColumnName="companyId", nullable = true)
	@OnDelete(action = OnDeleteAction.CASCADE)
    public Company getEndClient() {
		return endClient;
	}

	public void setEndClient(Company endClient) {
		this.endClient = endClient;
	}

	@ManyToOne(fetch = FetchType.LAZY, optional = false)
	@JoinColumn(name = "cons_id",referencedColumnName="consultantId", nullable = false)
	@OnDelete(action = OnDeleteAction.CASCADE)
    public Consultant getConsultant() {
        return consultant;
    }

    public void setConsultant(Consultant consultant) {
        this.consultant = consultant;
    }

    @Transient
	public Integer getMissionsCount() {
		return this.missions != null ? this.missions.size() : 0;
	}
	
	@OneToMany(mappedBy = "contract", cascade = CascadeType.ALL, fetch = FetchType.LAZY, orphanRemoval = true)
	public Set<Mission> getMissions() {
		return missions;
	}

	public void setMissions(Set<Mission> missions) {
		this.missions = missions;
	}

	@Override
    public String toString() {
        return "Contract [endDate=" + endDate + ", id=" + id + ", startDate=" + startDate + "]";
    }

}