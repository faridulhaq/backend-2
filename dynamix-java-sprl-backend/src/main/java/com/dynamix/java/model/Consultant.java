package com.dynamix.java.model;

import java.time.LocalDate;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.dynamix.java.util.Gender;
import com.dynamix.java.util.LocalDateDeserializer;
import com.dynamix.java.util.LocalDateSerializer;
import com.dynamix.java.util.MaritalStatus;
import com.dynamix.java.util.Title;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

@Entity
@Table(name = "consultant")
@JsonIgnoreProperties(ignoreUnknown = true)
public class Consultant {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long consultantId;

    @Column(name = "firstname", nullable = false)
    private String firstName;

    @Column(name = "lastname", nullable = false)
    private String lastName;

    @Column(columnDefinition = "Enum('" + Gender.Values.MALE + "', '" + Gender.Values.FEMALE + "')", name = "gender", nullable = false)
    private Gender gender;

    @Column(name = "email", nullable = false)
    private String email;

    @Column(name = "phone", nullable = false)
    private Long phone;

    @JsonDeserialize(using = LocalDateDeserializer.class)  
    @JsonSerialize(using = LocalDateSerializer.class) 
    @Column(name = "availability_date", nullable = false)
    private LocalDate availabilityDate;

    @Lob()
    @Column(name = "photo")
    private byte[] photo;

    @Column(columnDefinition = "Enum('" + Title.Values.ARCHITECT + "', '" + Title.Values.DEVELOPER + "', '" + Title.Values.SCRUMMASTER + "')", name = "title", nullable = false)
    private Title title;

    @Column(columnDefinition = "Enum('" + MaritalStatus.Values.DIVORCED + "', '" + MaritalStatus.Values.MARRIED + "', '" + MaritalStatus.Values.SINGLE + "', '" + MaritalStatus.Values.WIDOWED + "', '" + MaritalStatus.Values.WIDOWER + "')", name = "maritalstatus", nullable = false)
    private MaritalStatus maritalStatus;

    @Column(name = "linkedInUrl")
    private String linkedInUrl;

    @OneToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @JoinColumn(name = "resume_id")
    private Resume resume;

    @OneToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @JoinColumn(name = "address_id")
    private Address address;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "employeeId")
    private Employee employee;

    public Consultant() {

    }

    public long getConsultantId() {
        return consultantId;
    }

    public void setConsultantId(long consultantId) {
        this.consultantId = consultantId;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Gender getGender() {
        return gender;
    }

    public void setGender(Gender gender) {
        this.gender = gender;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Long getPhone() {
        return phone;
    }

    public void setPhone(Long phone) {
        this.phone = phone;
    }

    public LocalDate getAvailabilityDate() {
        return availabilityDate;
    }

    public void setAvailabilityDate(LocalDate availabilityDate) {
        this.availabilityDate = availabilityDate;
    }

    public byte[] getPhoto() {
        return photo;
    }

    public void setPhoto(byte[] photo) {
        this.photo = photo;
    }

    public Title getTitle() {
        return title;
    }

    public void setTitle(Title title) {
        this.title = title;
    }

    public MaritalStatus getMaritalStatus() {
        return maritalStatus;
    }

    public void setMaritalStatus(MaritalStatus maritalStatus) {
        this.maritalStatus = maritalStatus;
    }

    public String getlinkedInUrl() {
        return linkedInUrl;
    }

    public void setlinkedInUrl(String linkedInUrl) {
        this.linkedInUrl = linkedInUrl;
    }

    public Resume getResume() {
        return resume;
    }

    public void setResume(Resume resume) {
        this.resume = resume;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public Employee getEmployee() {
        return employee;
    }

    public void setEmployee(Employee employee) {
        this.employee = employee;
    }
}
