import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { EmployeeService } from './employee.service';
import { SelectItem } from 'primeng/api/selectitem';
import { Employee } from './employee';

@Component({
  selector: 'app-employees',
  templateUrl: './employee.component.html',
  styleUrls: ['./employee.component.css']
})
export class EmployeeComponent implements OnInit {

  employees : Employee[];

  sortOptions: SelectItem[];

  sortKey: string;

  sortField: string;

  sortOrder: number;

  availabilityDateRange: Date[];

  constructor( private employeeService: EmployeeService, private router: Router ) { }

  ngOnInit(): void {
    this.reloadData();
    this.sortOptions = [
      {label:'Name',value:'firstName'},
      {label:'Availability',value:'availabilityDate'}
    ]
  }

  
  selectEmployee(event: Event, employeeId: number) {
    this.router.navigate(['employees/employee',employeeId]);
    event.preventDefault();
}

reloadData() {
  this.employeeService.getEmployeesList().subscribe(employees => {
  this.employees = employees;
});

}

onSortChange(event) {
    let value = event.value;

    if (value.indexOf('!') === 0) {
        this.sortOrder = -1;
        this.sortField = value.substring(1, value.length);
    }
    else {
        this.sortOrder = 1;
        this.sortField = value;
    }
}

  addEmployee(event){
    this.router.navigate(['employees/employee',0]);
  }

}