import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { EmployeeComponent } from './employee.component';
import { EmployeeRoutingModule } from './employee-routing.module';
import { DataViewModule } from 'primeng/dataview';
import { PanelModule } from 'primeng/panel';
import { DropdownModule } from 'primeng/dropdown';
import { FormsModule } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';
import { InputTextModule } from 'primeng/inputtext';
import { SplitButtonModule } from 'primeng/splitbutton';
import { TabViewModule } from 'primeng/tabview';
import { TableModule } from 'primeng/table';
import { ButtonModule } from 'primeng/button';
import { AddEmployeeComponent } from './add-employee/add-employee.component';
import { CalendarModule } from 'primeng/calendar';
import { MessageModule } from 'primeng/message';
import { FileUploadModule } from 'primeng/fileupload';
import { OverlayPanelModule } from 'primeng/overlaypanel';
import { ConfirmationDialogComponent } from './confirmation-dialog/confirmation-dialog.component';
import {MatDialogModule} from '@angular/material/dialog';


@NgModule({
  declarations: [
    EmployeeComponent,
    AddEmployeeComponent,
    ConfirmationDialogComponent
  ],
  imports: [
    CommonModule,
    EmployeeRoutingModule,
    DataViewModule,
    PanelModule,
    DropdownModule,
    FormsModule,
    ReactiveFormsModule,
    InputTextModule,
    SplitButtonModule,
    TabViewModule,
    TableModule,
    ButtonModule,
    CalendarModule,
    MessageModule,
    FileUploadModule,
    OverlayPanelModule,
    MatDialogModule
  ]
})
export class EmployeeModule { }
