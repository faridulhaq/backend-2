import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ConsultantsComponent } from './consultants.component';
import { AddConsultantComponent } from './add-consultant/add-consultant.component';

const routes: Routes = [
  { path: '', component: ConsultantsComponent },
  { path: 'consultant/:consultantId', component: AddConsultantComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ConsultantsRoutingModule { }
