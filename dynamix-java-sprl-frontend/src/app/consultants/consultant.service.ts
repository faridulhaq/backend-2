import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ConsultantService {

  constructor(private http: HttpClient) { }

  getConsultants() : Observable<any>{
    return this.http.get('assets/consultants.json');
  }

  getConsultant( consultantId: number ): Observable<any> {
    //return this.http.get(`${environment.baseUrl}` + 'consultant' + `/${consultantId}`);
    return this.http.get('assets/consultants.json');
  }
}
