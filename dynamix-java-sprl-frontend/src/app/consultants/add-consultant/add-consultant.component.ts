import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import {  SelectItem, MenuItem } from 'primeng';
import { ConsultantService } from '../consultant.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Consultant } from 'src/app/models/consultant.interface';

@Component({
  selector: 'app-add-consultant',
  templateUrl: './add-consultant.component.html',
  styleUrls: ['./add-consultant.component.css']
})
export class AddConsultantComponent implements OnInit {

  consultantId: number;
  consultant: Consultant;
  consultantForm : FormGroup;
  submitted: boolean;
  actionItems : MenuItem[];
  genderOptions: SelectItem[];
  titleOptions: SelectItem[];
  maritalStatusOptions: SelectItem[];
  photoUpload : any[] = [];

  constructor(private route:ActivatedRoute, private router: Router,private fb: FormBuilder, private consultantService: ConsultantService) {
    this.genderOptions = [
      {label:'Male',value:'Male'},
      {label:'Female',value:'Female'}
    ];

    this. titleOptions = [
      {label:'Developer',value:'Developer'},
      {label:'ScrumMaster',value:'ScrumMaster'},
      {label:'Architect',value:'Architect'}
    ];

    this.maritalStatusOptions = [
      {label:'Single',value:'Single'},
      {label:'Married',value:'Married'},
      {label:'Divorced',value:'Divorced'},
      {label:'Widower',value:'Widower'},
      {label:'Widowed',value:'Widowed'}
    ];

    this.consultantId = this.route.snapshot.params['consultantId'];

    if( this.consultantId && this.consultantId != 0 ) {
        // this.consultantService.getConsultant(this.consultantId).subscribe(consultant => this.consultant = consultant.data );
        // this.consultantForm.patchValue(serverresponse);   
        
        //TODO remove below code after service integration
        this.consultant = {"consultantId":1,"firstName":"Bruce","lastName":"Wayne","email":"bruce@wayne.com","phone":"9134512345",
        "gender":"Male","availabilityDate":new Date("2020/06/29"),"title":"Developer","maritalStatus":"Single",
        "linkedInUrl":"http://linkedin.com",
        "address":{
        "addressId":1,"address1":"Wayne Tower","address2":"Gotham city","postalCode":12345,"city":"New York","country":"USA"
        }};
    }

    this.actionItems = [
      {
        label : 'Add', icon: 'pi pi-plus', command : () => this.addConsultant()
      },
      {
        label : 'Delete', icon: 'pi pi-times', command : () => this.delete()
      }      
    ];
  }


  ngOnInit(): void {
    this.consultantForm = this.fb.group({
      'firstName':new FormControl('',Validators.required),
      'lastName':new FormControl('',Validators.required),
      'gender':new FormControl('',Validators.required),
      'email':new FormControl('',[Validators.required,Validators.pattern("^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$") ]),
      'phone':new FormControl('',[ Validators.required,Validators.pattern('^[0-9]*$') ]),
      'address1':new FormControl('',Validators.required),
      'address2':new FormControl(''),
      'postalCode':new FormControl('',[ Validators.required,Validators.pattern('^[0-9]*$') ]),
      'country':new FormControl('',Validators.required),
      'city':new FormControl('',Validators.required),
      'photo':new FormControl(''),
      'title':new FormControl('',Validators.required),
      'maritalStatus':new FormControl('',Validators.required),
      'linkedInUrl':new FormControl(''),
      'availabilityDate':new FormControl('')
    });

    if(this.consultant != null) this.consultantForm.patchValue(this.consultant);
  }

  onSubmit(value: string) {
    this.submitted = true;
    //this.consultantService.createConsultant(value)
    //                      .subscribe(data => console.log(data), error => console.log(error));
    //this.messageService.add({severity:'info', summary:'Success', detail:'Form Submitted'});
  } 


  clearForm(event){
    this.consultantForm.reset();
  }

  get diagnostic() { return JSON.stringify(this.consultantForm.value); }

  onUploadEvent(event){
    for (let file of event.files) {
      this.photoUpload.push(file);
    }
    //this.messageService.add({ severity: 'info',summary: 'File Uploaded', detail: ''});
  }

  delete() {
  }

  backToConsultantList(){
    this.router.navigate(['consultants']);
  }

  addConsultant(){
    this.router.navigate(['consultants/consultant',0]);
  }
}
