import { Injectable } from '@angular/core';
import { HttpClient, HttpParams, HttpHeaders } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { map } from 'rxjs/operators';
import { Subject } from 'rxjs';
import { Router } from '@angular/router';

@Injectable()
export class AuthenticationService {

  USER_NAME = 'user';
  TOKEN = 'token';
  public authenticationResult = new Subject();

  constructor(private router: Router, private httpClient: HttpClient) {
    
   }

  // Store JWT token in session once authentication is successful
  authenticate(uName, pswd) {
    const body = new HttpParams()
      .set('username', uName)
      .set('password', pswd)
      .set('grant_type', 'password');

    return this.httpClient
      .post<any>(`${environment.auth_baseurl}` + 'token', body.toString())
      .pipe(map((data) => {
        this.registerData(data.access_token, uName);
      }));
  }

  registerData(token: string, uName: string) {
    window.localStorage.setItem(this.USER_NAME, uName);
    window.localStorage.setItem(this.TOKEN, token);
  }

  getToken(): String {
    return window.localStorage['token'];
  }

  logout() {
    window.localStorage.removeItem(this.USER_NAME);
    window.localStorage.removeItem(this.TOKEN);
    this.setAuthentication(false);
    this.router.navigate(['/login']);
  }

  isUserLoggedIn() {
    let token = window.localStorage.getItem(this.TOKEN)
    if (token === null) return false
    return true
  }

  getLoggedInUserName() {
    let user = window.localStorage.getItem(this.USER_NAME)
    if (user === null) return ''
    return user
  }

  setAuthentication(value: boolean) {
    this.authenticationResult.next(value);
  }

}
