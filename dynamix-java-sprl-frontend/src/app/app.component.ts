import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthenticationService } from './core/services/authentication.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'DYNADMIN';

  isUserLoggedIn: boolean;

  constructor(private router: Router,
    private authenticationService: AuthenticationService) {
      this.isUserLoggedIn = this.authenticationService.isUserLoggedIn();
  }

  logout() {
    this.authenticationService.logout();
  }

  ngOnInit() {    
    this.authenticationService.authenticationResult.subscribe((res) => {
      if (res) {
        this.isUserLoggedIn = true;
      }
      else{
        this.isUserLoggedIn = false;
      }
    });
  }


}
